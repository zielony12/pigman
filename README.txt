Pigman is a simple package manager.

This package manager doesn't do anything besides installing packages (yet).

compile without debug symbols, with gcc optimizations:
	`RELEASE=1 make`

example package tree:

example_package/
├── PLIST
├── binary
├── license
└── library

example PLIST file:
```
binary /usr/bin/binary 0755
license /usr/share/licenses/example/license 0644
library /usr/lib/library 0755
```

TODO: local package db and uninstallation.

_________
zielony12
