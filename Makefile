CCFLAGS := -g -Wall -Werror -Wpedantic -Wunused-result

RELEASE ?= 0

ifeq ($(RELEASE), 1)
	CCFLAGS := -O2
endif

all: pigman_add

pigman_add.o: pigman_add.c
	$(CC) -c -o $@ $< $(CCFLAGS)

pigman_add: pigman_add.o
	$(CC) -o $@ $< -ltar

clean:
	rm *.o
	rm pigman_add
