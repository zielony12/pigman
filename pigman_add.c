#include <stdio.h>
#include <sys/stat.h>
#include <pwd.h>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <string.h>
#include <stdlib.h>
#include <libtar.h>
#include <unistd.h>
#include <stdbool.h>
#include <errno.h>
#include <fcntl.h>
#include <libgen.h>

#include "pigman_add.h"

char **splitstr(char *str, char *sep) {
	char **ret = malloc(sizeof(char *));
	if(!ret)
		return NULL;
	int i = 0;
	int sz = 1;
	char *token = strtok(str, sep);
	while(token) {
		ret[i] = strdup(token);
		token = strtok(NULL, sep);
		i++;
		if(i >= sz) {
			sz++;
			ret = realloc(ret, sz * sizeof(char *));
			if(!ret)
				return NULL;
		}
	}
	ret[i] = NULL;

	return ret;
}

int ptrlen(const char *ptr) {
	int size = 0;

	while(*ptr) {
		size++;
		ptr++;
	}

	return size;
}

// FIXME: It doesn't attempt to open and make a dir
// if there is no slash afterwards.
void mkdir_r(char *path, int mode) {
	int len = ptrlen(path);
	char *buf = malloc((len + 1) * sizeof(char));
	for(int i = 0; i < len; i++) {
		buf[i] = path[i];
		if(path[i] == '/') {
			buf[i + 1] = '\0';
			DIR *ds = opendir(buf);
			if(!ds)
				mkdir(buf, mode);
			closedir(ds);
		}
	}
	free(buf);
}

int error(int code, char *msg) {
	fputs(msg, stderr);
	return code;
}

void usage() {
	puts("usage: pigman_add -f file_path [-p prefix]\n");
}

// TODO: use a ret value instead of
// immediately calling error(). Make
// sure all resources are free'd.
int main(int argc, char *argv[]) {
	int opt;
	int fflag = false;

	char *file_path;
	char *prefix = "/";

	while((opt = getopt(argc, argv, "f:p:")) != -1) {
		switch(opt) {
		case 'f':
			file_path = optarg;
			fflag = true;
			break;
		case 'p':
			prefix = optarg;
			break;
		default:
			usage();
			return 1;
		}
	}

	if(!fflag) {
		usage();
		return 1;
	}

	if(!test_file(file_path, "r"))
		return error(2, "Unable to read specified file.\n");
	
	if(!regular_file(file_path))
		return error(2, "Not a regular file.\n");
	
	struct passwd *pw = getpwuid(getuid());
	char *home_dir = pw->pw_dir;
	
	char *cache_subdir = "/.cache/";
	char *pkgman_subdir = "pigman/";

	char *cache_dir = malloc((ptrlen(home_dir) + ptrlen(cache_subdir) + ptrlen(pkgman_subdir) + 1) * sizeof(char));
	strcpy(cache_dir, home_dir);
	strcat(cache_dir, cache_subdir);

	mkdir_r(cache_dir, 0700);

	// separate cuz chmod differs
	strcat(cache_dir, pkgman_subdir);

	mkdir_r(cache_dir, 0755);

	TAR *tar;

	if(tar_open(&tar, file_path, NULL, O_RDONLY, 0, TAR_GNU) == -1 ||
	   tar_extract_all(tar, cache_dir) == -1) {
		free(cache_dir);
		tar_close(tar);
		return error(2, "Unable to open specified file as TAR archive.\n");
	}

	char *dir_name = basename(file_path);
	int dir_name_len = ptrlen(dir_name);

	for(int i = 0; i < dir_name_len; i++) {
		if(i > dir_name_len - 5) {
			dir_name[i] = '\0';
		}
	}

	char *pkg_dir = malloc((ptrlen(cache_dir) + ptrlen(dir_name) + 1 + 1) * sizeof(char));
	strcpy(pkg_dir, cache_dir);
	strcat(pkg_dir, dir_name);
	strcat(pkg_dir, "/");

	free(cache_dir);

	char *plist_name = "PLIST";
	
	char *plist = malloc((ptrlen(pkg_dir) + ptrlen(plist_name) + 1) * sizeof(char));
	strcpy(plist, pkg_dir);
	strcat(plist, plist_name);
	FILE *fp = fopen(plist, "r");

	if(!fp)
		return error(3, "Package does not contain the PLIST file.\n");

	char line[128];

	while(fgets(line, 128, fp)) {
		if(strcmp(line, "") == 0)
			continue;
		char **tokens = splitstr(line, " \n");
		
		char *src = malloc((ptrlen(pkg_dir) + ptrlen(tokens[0]) + 1) * sizeof(char));
		strcpy(src, pkg_dir);
		strcat(src, tokens[0]);
		
		char *dst = malloc((ptrlen(prefix) + ptrlen(tokens[1]) + 1) * sizeof(char));
		strcpy(dst, prefix);
		strcat(dst, tokens[1]);

		//printf("Installing %s to %s with mode %d.\n", src, dst, atoi(tokens[2]));
		int ret = 0;
		char *mode;
		unsigned long ul_mode = strtoul(tokens[2], &mode, 8);
		ret = install(src, dst, (mode_t)ul_mode);
		
		free(src);
		free(dst);

		for(int i = 0; tokens[i] != NULL; i++)
			free(tokens[i]);
		free(tokens);

		if(ret > 0)
			error(3, "install() failed.\n");
	}

	free(plist);
	free(pkg_dir);
	tar_close(tar);
	fclose(fp);

	return 0;
}

bool regular_file(char *file_path) {
	struct stat sb;
	stat(file_path, &sb);
	
	if((sb.st_mode & S_IFMT) != S_IFREG)
		return false;
	return true;
}

bool test_file(char *file_path, char *mode) {
	FILE *fp = fopen(file_path, mode);
	if(!fp)
		return false;
	fclose(fp);
	return true;
}

int install(char *src, char *dst, mode_t mode) {
	int ret = 0;
	//int c;
	char c[1024];

	char *dir = strdup(dst);
	dirname(dir);
	char *base_dir = malloc((ptrlen(dir) + 1 + 1) * sizeof(char));
	strcpy(base_dir, dir);
	strcat(base_dir, "/");

	mkdir_r(base_dir, 0755);

	printf("(%o) %s\n", mode, dst);

	FILE *fdr = fopen(src, "r");
	FILE *fdw = fopen(dst, "w");

	if(!fdr || !fdw) {
		ret = 1;
		goto install_free;
	}

	//while((c = fgetc(fdr)) != EOF)
	//	fputc(c, fdw);

	while(!feof(fdr)) {
		size_t bytes = fread(c, 1, sizeof(c), fdr);
		if(bytes)
			fwrite(c, 1, bytes, fdw);
	}

	chmod(dst, mode);
install_free:
	if(fdr)
		fclose(fdr);
	if(fdw)
		fclose(fdw);

	free(dir);
	free(base_dir);

	return ret;
}
