#pragma once

char **splitstr(char *, char *);
void mkdir_r(char *, int);
int ptrlen(const char *);
int ptrptrlen(char **);
int error(int, char *);
void usage();
bool regular_file(char *);
bool test_file(char *, char *);
int install(char *, char *, mode_t);
